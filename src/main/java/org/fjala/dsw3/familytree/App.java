package org.fjala.dsw3.familytree;

import javafx.application.Application;
import javafx.stage.Stage;
import org.fjala.dsw3.familytree.controller.ProjectManagerController;
import org.fjala.dsw3.familytree.view.ProjectManagerView;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        ProjectManagerView view = new ProjectManagerView(primaryStage);
        ProjectManagerController controller = new ProjectManagerController(view);

        controller.startView();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
