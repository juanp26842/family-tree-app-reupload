package org.fjala.dsw3.familytree.controller;

import java.io.File;

import org.fjala.dsw3.familytree.model.FamilyTreeProject;
import org.fjala.dsw3.familytree.services.DirectoryWriter;
import org.fjala.dsw3.familytree.view.CreationProjectForm;

/**
 * A {@code CreationProjectController} validates the data entered in a form
 * for the creation of a new family tree project.
 */
public class CreationProjectController {

    private final CreationProjectForm form;
    private final ProjectManagerController projectManagerController;
    private final DirectoryWriter directoryWriter = new DirectoryWriter();
    private File directory;

    /**
     * Creates a new {@code CreationProjectController} with the specified form.
     * @param form      the form for the creation of a new project
     */
    public CreationProjectController(ProjectManagerController projectManagerController, CreationProjectForm form) {
        this.projectManagerController = projectManagerController;
        this.form = form;
        this.setCreateAction();
        this.setCancelAction();
        this.setCreateActionButtonChangeDirectory();
    }

    /**
     * The information from the form fields is collected to validate them,
     * and if everything is valid, the new project is displayed.
     */
    private void setCreateAction() {
        this.form.setCreateAction(event -> {
            String nameOfNewProject = form.getNameOfNewProject();
            String descriptionOfNewProject = form.getDescriptionOfNewProject();
            FamilyTreeProject project;

            try {
                project = this.projectManagerController.createProject(nameOfNewProject, descriptionOfNewProject);
                this.form.close();
                this.projectManagerController.openProject(project);
            } catch (Exception ignored) {

            }
        });
    }

    /**
     * The information from the form fields is collected to validate them,
     * and if everything is valid, the new project is displayed.
     */
    private void setCancelAction() {
        this.form.setCancelAction(event -> {
            this.form.close();
        });
    }

    /** In this method collect the directory from directory chooser to set the new path */
    private void setCreateActionButtonChangeDirectory() {
        this.form.setCreateActionButtonChangeDirectory(event -> {
            form.createDirectoryChooser();
            if (form.getDirectoryFromDirectoryChooser() != null) {
                directory = form.getDirectoryFromDirectoryChooser();
                form.setDirectoryPath(directory.getAbsolutePath());
            }
        });
    }

    public void startView() {
        directory = directoryWriter.createPredeterminateDirectory();
        form.setDirectoryPath(directory.getAbsolutePath());
        this.form.start();
    }
}
