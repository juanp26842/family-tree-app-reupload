package org.fjala.dsw3.familytree.controller;

import org.fjala.dsw3.familytree.view.FamilyTreeMenu;
import org.fjala.dsw3.familytree.view.FormToAddRootMember;

public class FamilyTreeMenuController {

    private final FamilyTreeMenu familyTreeMenu;

    public FamilyTreeMenuController(FamilyTreeMenu familyTreeMenu) {
        this.familyTreeMenu = familyTreeMenu;
        setAddANewMemberButtonAction();
    }

    private void setAddANewMemberButtonAction() {
        familyTreeMenu.setAddANewMemberButtonAction(event -> {
            FormToAddRootMember formToAddRootMember = new FormToAddRootMember(familyTreeMenu.getStage());
            FormToAddRootMemberController formToAddRootMemberController = new FormToAddRootMemberController(formToAddRootMember);
            formToAddRootMemberController.startView();
        });
    }

    public void startView() {
        this.familyTreeMenu.start();
    }
}
