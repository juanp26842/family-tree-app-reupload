package org.fjala.dsw3.familytree.controller;

import org.fjala.dsw3.familytree.exceptions.ValidationException;
import org.fjala.dsw3.familytree.model.Member;
import org.fjala.dsw3.familytree.view.Alerts;
import org.fjala.dsw3.familytree.view.FamilyTreeMenu;
import org.fjala.dsw3.familytree.view.FormToAddRootMember;

public class FormToAddRootMemberController {

    private final FormToAddRootMember formToAddRootMember;

    public FormToAddRootMemberController(FormToAddRootMember formToAddRootMember) {
        this.formToAddRootMember = formToAddRootMember;
        setCreateMemberButtonAction();
        setCancelButtonAction();
    }

    private void setCancelButtonAction() {
        formToAddRootMember.setCancelButtonAction(event -> formToAddRootMember.getStage().close());
    }

    private void setCreateMemberButtonAction() {
        formToAddRootMember.setCreateMemberButtonAction(event -> {
            try {
                Member newMember = new Member(formToAddRootMember.getNameField().getText().trim(),
                        formToAddRootMember.getLastNameField().getText().trim(),
                        formToAddRootMember.getGenderChoiceBox().getValue(), formToAddRootMember.getDatePicker().getValue());
                Alerts.alertMemberAddedCorrectly();
                formToAddRootMember.getStage().close();
                FamilyTreeMenu familyTreeMenu = new FamilyTreeMenu(formToAddRootMember.getParentStage(), newMember);
                FamilyTreeMenuController familyTreeMenuController = new FamilyTreeMenuController(familyTreeMenu);
                familyTreeMenuController.startView();
            } catch (ValidationException e) {
                Alerts.alertIncorrectData(e.getFieldName());
            }
        });
    }

    public void startView() {
        this.formToAddRootMember.start();
    }

}
