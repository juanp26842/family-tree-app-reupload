package org.fjala.dsw3.familytree.controller;

import org.fjala.dsw3.familytree.model.FamilyTreeProject;
import org.fjala.dsw3.familytree.view.ProjectView;

/**
 * A {@code ProjectController} is the intermediary between the actions the user wants
 * to perform on a project and how they will be executed
 */
public class ProjectController {

    private final ProjectView view;
    private FamilyTreeProject model;

    /**
     * Creates a new {@code CreationProjectController} with the specified view
     * @param view      the view that will contain the current project
     */
    public ProjectController(ProjectView view) {
        this.view = view;
    }

    public void startView() {
        this.view.start();
    }

    public void setProject(FamilyTreeProject project) {
        this.model = project;
    }
}
