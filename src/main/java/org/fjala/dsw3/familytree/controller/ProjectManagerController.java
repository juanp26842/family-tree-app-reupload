package org.fjala.dsw3.familytree.controller;

import org.fjala.dsw3.familytree.model.FamilyTreeProject;
import org.fjala.dsw3.familytree.view.CreationProjectForm;
import org.fjala.dsw3.familytree.view.ProjectInfoView;
import org.fjala.dsw3.familytree.view.ProjectManagerView;

/**
 * A {@code ProjectManagerController} is an intermediary for the management
 * and creation of projects and how they will be presented to the user
 */
public class ProjectManagerController {

    private final ProjectManagerView view;

    /**
     * Creates a new {@code ProjectManagerController} with the specified view
     * @param view      the view that will contain the current projects
     */
    public ProjectManagerController(ProjectManagerView view) {
        this.view = view;
        this.setCreateProjectAction();
    }

    /**
     * Sets that the form for the creation of the project shall be launched
     */
    private void setCreateProjectAction() {

        CreationProjectForm form = new CreationProjectForm(view.getStage());
        CreationProjectController creationProjectController = new CreationProjectController(
                this, form);

        this.view.setCreateProjectAction(event -> creationProjectController.startView());
    }

    public void startView() {
        this.view.start();
    }

    /**
     * For the creation of the project, the name must be validated
     * If the name is not valid, an exception will be thrown
     * @param name          of the new project
     * @param description   of the new project
     * @return              A {@code FamilyTreeProject} if the name is valid
     *                      otherwise an exception will be thrown
     */
    public FamilyTreeProject createProject(String name, String description) {
        // TODO: Add name validation
        return new FamilyTreeProject(name, description);
    }

    /**
     * To open a project, the project is set in the project controller
     * and the project view is displayed
     * @param project       to be opened
     */
    public void openProject(FamilyTreeProject project) {
        ProjectInfoView projectInfoView = new ProjectInfoView(view.getStage(), project);
        projectInfoView.start();
    }
}
