package org.fjala.dsw3.familytree.exceptions;

public class BlankStringException extends ValidationException {

    private static final String DEFAULT_MESSAGE = "The field must not be a blank value.";

    public BlankStringException(String fieldName) {
        super(DEFAULT_MESSAGE, fieldName);
    }
}
