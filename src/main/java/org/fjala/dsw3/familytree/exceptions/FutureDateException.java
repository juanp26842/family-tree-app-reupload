package org.fjala.dsw3.familytree.exceptions;

public class FutureDateException extends ValidationException {

    private static final String DEFAULT_MESSAGE = "The field must not be a future date.";

    public FutureDateException(String fieldName) {
        super(DEFAULT_MESSAGE, fieldName);
    }
}
