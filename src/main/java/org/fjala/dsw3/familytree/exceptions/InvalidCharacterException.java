package org.fjala.dsw3.familytree.exceptions;

public class InvalidCharacterException extends ValidationException {
    
    private static final String DEFAULT_MESSAGE = "The field must not contain special characters.";

    public InvalidCharacterException(String fieldName) {
        super(DEFAULT_MESSAGE, fieldName);
    }
}
