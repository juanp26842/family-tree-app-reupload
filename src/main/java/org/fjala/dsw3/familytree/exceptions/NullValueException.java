package org.fjala.dsw3.familytree.exceptions;

public class NullValueException extends ValidationException {
    
    private static final String DEFAULT_MESSAGE = "The field must be filled out.";

    public NullValueException(String fieldName) {
        super(DEFAULT_MESSAGE, fieldName);
    }
}
