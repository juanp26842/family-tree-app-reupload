package org.fjala.dsw3.familytree.exceptions;

public class ValidationException extends IllegalArgumentException {

    private final String fieldName;

    public ValidationException(String message, String fieldName) {
        super(message);
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}
