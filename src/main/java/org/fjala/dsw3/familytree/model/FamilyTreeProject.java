package org.fjala.dsw3.familytree.model;

import org.fjala.dsw3.familytree.utils.Validations;

/**
 *This is the Family Tree Project class model.
 */
public class FamilyTreeProject {
    private String name;
    private String description;
    private Member rootMember;

    /**
     *This is the Constructor.
     * @param name this stores the family name.
     * @param description this stores the family description.
     */
    public FamilyTreeProject(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setName(String name) {
        validate(name, "Name");
        this.name = name;
    }

    private void validate(String string, String fieldName) {
        Validations.validateNonNullValue(string, fieldName);
        Validations.validateNotBlankString(string, fieldName);
        Validations.validateValidCharacters(string, fieldName);
    }

    /**
     *This method is to get the name.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *This method is to get the description.
     * @return description
     */
    public String getDescription() {
        return description;
    }

    public void setRootMember(Member member) {
        this.rootMember = member;
    }

    public Member getRootMember() {
        return rootMember;
    }
}
