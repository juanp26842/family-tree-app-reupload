package org.fjala.dsw3.familytree.model;

public enum Gender {
    MALE,
    FEMALE
}
