package org.fjala.dsw3.familytree.model;

import java.time.LocalDate;
import org.fjala.dsw3.familytree.utils.Validations;

public class Member {
    
    private final static String NAME_FIELD = "Name";
    private final static String LAST_NAME_FIELD = "Last name";    
    private final static String BIRTH_DATE_FIELD = "BirthDate";
    private final static String GENDER_FIELD = "Gender";

    private String name;
    private String lastName;    
    private LocalDate birthDate;
    private Gender gender;

    public Member(String name, String lastName, Gender gender, LocalDate birthDate) {
        checkAttributes(name, lastName, birthDate, gender);
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Gender getGender() {
        return gender;
    }
    
    private void checkNames(String name, String field) {
        Validations.validateNonNullValue(name, field);
        Validations.validateNotBlankString(name, field);
        Validations.validateValidCharacters(name, field);
    }    
    
    private void checkAttributes(String name, String lastName, LocalDate birthDate, Gender gender) {
        checkNames(name, NAME_FIELD);
        checkNames(lastName, LAST_NAME_FIELD);
        Validations.validateNonNullValue(gender, GENDER_FIELD);
        if (birthDate != null) {
            Validations.validateNotFutureDate(birthDate, BIRTH_DATE_FIELD);
        }
    }
}