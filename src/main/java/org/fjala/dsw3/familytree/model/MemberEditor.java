package org.fjala.dsw3.familytree.model;

public class MemberEditor {

    private Member member;

    public MemberEditor() {

    }

    public MemberEditor(Member member) {
        this.member = member;
    }

    public void setMember(Member member) {
        throw new UnsupportedOperationException();
    }

    public void editMembersName(String name) {
        throw new UnsupportedOperationException();
    }

    public void editMembersLastName(String lastName) {
        throw new UnsupportedOperationException();
    }
}
