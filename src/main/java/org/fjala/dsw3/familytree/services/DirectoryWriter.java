package org.fjala.dsw3.familytree.services;

import java.io.File;

public class DirectoryWriter {

    private final static String DIRECTORY_NAME = "Family Tree Projects";
    private final static String DIRECTORY_HOME_USER = System.getProperty("user.home");

    /**
     * The predeterminate directory it's created in the home user with the name
     * Family Tree Projects
     * 
     * @return path of the predeterminate directory
     */
    public File createPredeterminateDirectory() {

        File directory = new File(DIRECTORY_HOME_USER + "/" + DIRECTORY_NAME);

        if (!(isPredeterminateDirectoryCreated(directory))) {
            directory.mkdirs();
        }

        return directory;
    }

    /**
     * Verify if the predeterminate directory was created in the correct path
     * 
     * @param directory FIle directory
     * @return true if the predeterminate directory was created in the correct path and false if not
     */
    private boolean isPredeterminateDirectoryCreated(File directory) {
        return directory.isDirectory() && directory.exists();
    }
    
}
