package org.fjala.dsw3.familytree.services;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.time.LocalDate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.fjala.dsw3.familytree.model.FamilyTreeProject;

public class JsonFileWriter {

    private final static String SPECIAL_CHARACTERS_REGEX = "[^\\p{L}\\p{Z}]";
    private static final String JSON_EXTENSION = ".json";
    private static final String SLASH = "/";

    private FamilyTreeProject tree;

    private Path completePath;

    /**
     * Instantiates a new Json file writer.
     * @param tree the tree
     * @param path the path
     */
    public JsonFileWriter(FamilyTreeProject tree, String directorypath) {
        this.tree = tree;
        completePath = FileSystems.getDefault().getPath(directorypath + fixJsonFileName(tree.getName()));
    }

    /**
     * Gets path.
     * @return the path
     */
    public String getPath() {
        return completePath.toString();
    }

    /**
     * Save family tree project.
     * @throws IOException the io exception
     */
    public void saveFamilyTreeProject() throws IOException {
        Gson gson = new GsonBuilder().
                registerTypeAdapter(LocalDate.class, new LocalDateSerializer()).setPrettyPrinting().create();

        if (!isFileValid()) {
            throw new IOException("The file with the specified path does not exist, or is not a json file.");
        }
        File file = completePath.toFile();
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(gson.toJson(tree));
        fileWriter.close();
    }

    /**
     * Is file valid boolean.
     * @return the boolean
     */
    private boolean isFileValid() {
        return completePath.toFile().getName().endsWith(JSON_EXTENSION);
    }

    /**
     * Fix the Json file name without spaces, special Characters and add the json extension
     * and add the slash to save the Json File
     * @param nameOfFile Json file name
     * @return the Json file name fixed without spaces, special Characters and add the json extension 
     * and the slash to save the Json File
     */
    public String fixJsonFileName(String nameOfFile) {
        String jsonFileNameWithoutSpaces = deleteSpaces(nameOfFile);
        String jsonFileNameWithoutCharacters = deleteSpecialCharacters(jsonFileNameWithoutSpaces);
        return SLASH + jsonFileNameWithoutCharacters + JSON_EXTENSION;
    }

    /**
     * Delete spaces, tabs and carriage returns
     * @param nameOfFile Json file name 
     * @return Json file name without spaces, tabs and carriage returns
     */
    public String deleteSpaces(String nameOfFile) {
        return nameOfFile.replaceAll("\\s", "");
    }

    /**
     * Delete special characters in a string
     * @param nameOfFile is a string of name a file
     * @return the string converted without special characters
     */
    public String deleteSpecialCharacters(String nameOfFile) {
        return nameOfFile.replaceAll(SPECIAL_CHARACTERS_REGEX,"");
    }

}