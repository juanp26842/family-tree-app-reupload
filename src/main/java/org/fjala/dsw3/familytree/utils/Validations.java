package org.fjala.dsw3.familytree.utils;

import org.fjala.dsw3.familytree.exceptions.BlankStringException;
import org.fjala.dsw3.familytree.exceptions.FutureDateException;
import org.fjala.dsw3.familytree.exceptions.InvalidCharacterException;
import org.fjala.dsw3.familytree.exceptions.NullValueException;

import java.time.LocalDate;

public class Validations {

    private final static String SIGN_REGEX = ".*[{}()\\[\\]=+*_\\-/\\\\<>\\|?%$!].*";
        
    public static void validateNonNullValue(Object object, String fieldName) {
        if (object == null) {
            throw new NullValueException(fieldName);
        }
    }

    public static void validateNotFutureDate(LocalDate date, String fieldName) {
        if (date.isAfter(LocalDate.now())) {
            throw new FutureDateException(fieldName);
        }
    }

    public static void validateNotBlankString(String string, String fieldName) {
        if (string.isBlank()) {
            throw new BlankStringException(fieldName);
        }
    }

    public static void validateValidCharacters(String string, String fieldName) {
        if (string.matches(SIGN_REGEX)) {
            throw new InvalidCharacterException(fieldName);
        }
    }
}
