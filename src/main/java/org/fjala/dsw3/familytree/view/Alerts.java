package org.fjala.dsw3.familytree.view;

import javafx.scene.control.Alert;

public class Alerts {

    private static final String INCORRECT_DATA_TITLE = "The data entered is incorrect";
    private static final String MEMBER_ADDED_CORRECTLY_TITLE = "The data entered is incorrect";
    private static final String MEMBER_ADDED_CORRECTLY_CONTENT = "The data entered is incorrect";

    public static void alertIncorrectData(String fieldName) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        String content = "Please verify the " + fieldName;
        alert.setHeaderText(null);
        alert.setTitle(INCORRECT_DATA_TITLE);
        alert.setContentText(content);
        alert.showAndWait();
    }

    public static void alertMemberAddedCorrectly() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle(MEMBER_ADDED_CORRECTLY_TITLE);
        alert.setContentText(MEMBER_ADDED_CORRECTLY_CONTENT);
        alert.showAndWait();
    }
}
