package org.fjala.dsw3.familytree.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmationPopUpView {

    public static final String CONFIRMATION_TITLE = "Edit confirmation";
    public static final String CONFIRMATION_MESSAGE = "Are you sure you want to save these changes?";
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final int HEIGHT = 60;
    public static final int WIDTH = 300;

    private final Label confirmationLabel = new Label();
    private final Button yes = new Button(YES);
    private final Button no = new Button(NO);

    private final Stage form = new Stage();
    private final VBox mainPane = new VBox();
    private final BorderPane buttonPane = new BorderPane();
    private final Stage parentStage;
    private final ProjectInfoEditForm changesEditForm;

    public ConfirmationPopUpView(Stage parentStage, ProjectInfoEditForm projectInfoEditForm) {
        this.parentStage = parentStage;
        this.changesEditForm = projectInfoEditForm;
        initComponents();
    }

    private void initComponents() {
        confirmationLabel.setText(CONFIRMATION_MESSAGE);
        confirmationLabel.setWrapText(true);

        yes.setOnAction(event -> {
            changesEditForm.confirmChanges();
            form.close();
        });

        no.setOnAction(event -> {
            changesEditForm.revertChanges();
            form.close();
        });

        buttonPane.setRight(yes);
        buttonPane.setLeft(no);

        mainPane.setAlignment(Pos.BOTTOM_CENTER);
        mainPane.setSpacing(10);
        mainPane.getChildren().addAll(confirmationLabel, buttonPane);
        mainPane.setBorder(new Border(new BorderStroke(Color.TRANSPARENT,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));

        form.setResizable(false);
        form.initModality(Modality.APPLICATION_MODAL);
        form.initOwner(parentStage);
        form.setScene(new Scene(mainPane, WIDTH, HEIGHT));
        form.setTitle(CONFIRMATION_TITLE);
    }

    public void start() {
        this.form.show();
    }
}
