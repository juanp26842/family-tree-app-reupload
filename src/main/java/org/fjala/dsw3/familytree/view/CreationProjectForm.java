package org.fjala.dsw3.familytree.view;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 * A {@code CreationProjectForm} represents the form with the fields necessary
 * for the creation of a new family tree project.
 */
public class CreationProjectForm {

    public static final String FORM_TITLE = "New Project";
    public static final String PROJECT_NAME = "Project name";
    public static final String DESTINATION_DIRECTORY_PATH = "Destination path";
    public static final String PROJECT_DESCRIPTION = "Project description";
    public static final String CHANGE_DIRECTORY = "Change directory";
    public static final String CANCEL = "Cancel";
    public static final String CREATE = "Create";
    public static final int HEIGHT = 500;
    public static final int WIDTH = 400;
    private static final String CSS_PATH = "/css/creation-project-form.css";
    private static final String FIELDS_ID = "project-fields";
    private static final String FOOTER_ID = "footer-section";

    private final Label name = new Label(PROJECT_NAME);
    private final Label destinationDirectoryPath = new Label(DESTINATION_DIRECTORY_PATH);
    private final Label description = new Label(PROJECT_DESCRIPTION);
    private final TextField nameField = new TextField();
    private final TextField destinationDirectoryPathField = new TextField();
    private final TextArea descriptionField = new TextArea();
    private final Button cancel = new Button(CANCEL);
    private final Button create = new Button(CREATE);
    private final Button changeDirectory = new Button(CHANGE_DIRECTORY);



    private final BorderPane mainPane = new BorderPane();
    private final VBox fieldsPane = new VBox();
    private final BorderPane buttonsPane = new BorderPane();
    private final Stage form = new Stage();
    private final Stage parentStage;
    private final Scene scene = new Scene(mainPane, WIDTH, HEIGHT);
    private final HBox directoryPane = new HBox();

    private String directoryPath;
    private File directoryFromDirectoryChooser;

    /**
     * Creates a new {@code CreationProjectForm} and initializes the UI components.
     * @param parentStage   main stage
     */
    public CreationProjectForm(Stage parentStage) {
        this.parentStage = parentStage;
        this.initComponents();
    }

    /**
     * Adds the fields and buttons in an orderly fashion to the main panel
     * and this is added to the form.
     */
    private void initComponents() {
        scene.getStylesheets().add(getClass().getResource(CSS_PATH).toExternalForm());
        fieldsPane.setId(FIELDS_ID);
        fieldsPane.getChildren().addAll(name, nameField, description, descriptionField,
                destinationDirectoryPath, directoryPane);

        directoryPane.getChildren().addAll(destinationDirectoryPathField, changeDirectory);
        destinationDirectoryPathField.setEditable(false);

        buttonsPane.setId(FOOTER_ID);
        buttonsPane.setLeft(cancel);
        buttonsPane.setRight(create);

        mainPane.setCenter(fieldsPane);
        mainPane.setBottom(buttonsPane);

        form.initModality(Modality.APPLICATION_MODAL);
        form.initOwner(parentStage);
        form.setScene(scene);
        form.setTitle(FORM_TITLE);
    }

    /**
     * The form is made visible on the user's screen
     * and the fields are cleaned
     */
    public void start() {
        this.nameField.clear();
        this.descriptionField.clear();
        this.form.show();
    }

    /**
     * Closes this form
     */
    public void close() {
        this.form.close();
    }

    /**
     * The action to be performed by the create button is set
     * @param event     to be assigned to the create button
     */
    public void setCreateAction(EventHandler<ActionEvent> event) {
        this.create.setOnAction(event);
    }

    /**
     * The action to be performed by the cancel button is set
     * @param event     to be assigned to the cancel button
     */
    public void setCancelAction(EventHandler<ActionEvent> event) {
        this.cancel.setOnAction(event);
    }

    /**
     * The action to be performed by the change directory button is set
     * @param event     to be assigned to the change directory button
     */
    public void setCreateActionButtonChangeDirectory(EventHandler<ActionEvent> event) {
        this.changeDirectory.setOnAction(event);
    }

    /**
     * Returns what is in the text field containing the name for the new project
     * @return the name of the new project
     */
    public String getNameOfNewProject() {
        return nameField.getText();
    }

    public Stage getStage(){
        return this.form;
    }

    /**
     * Returns what is in the text field containing the description for the new project
     * @return the description of the new project
     */
    public String getDescriptionOfNewProject() {
        return descriptionField.getText();
    }

    /** Create the directory chooser to select a directory */
    public void createDirectoryChooser() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryFromDirectoryChooser = directoryChooser.showDialog(form);
    }

    /**
     * Returns the directory selected by the user from directory chooser 
     * @return Directory from directory chooser
     */
    public File getDirectoryFromDirectoryChooser() {
        return directoryFromDirectoryChooser;
    }

    /**
     * Set the default path string for insert in the destinationPath field
     * @param directoryPath the directory Path of the project
     */
    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
        destinationDirectoryPathField.setText(this.directoryPath);
    }
}
