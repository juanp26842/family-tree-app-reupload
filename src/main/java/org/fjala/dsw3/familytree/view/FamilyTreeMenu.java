package org.fjala.dsw3.familytree.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.fjala.dsw3.familytree.model.Member;

public class FamilyTreeMenu {

    private static final String ADD_A_NEW_MEMBER_BUTTON_NAME = "+ Add a new member";
    private static final int WIDTH = 640;
    private static final int HEIGHT = 640;
    private static final String CSS_PATH = "/css/family-tree-menu.css";
    private static final String SCROLL_PANE_ID = "scroll-pane";
    private static final String ANCHOR_PANE_ID = "anchor-pane";
    private static final String CONTAINER_ELEMENTS_ID = "container-elements";

    private final Member root;
    private final ScrollPane scrollPane = new ScrollPane();
    private final VBox containerElements = new VBox(16);
    private final Button addANewMemberButton = new Button(ADD_A_NEW_MEMBER_BUTTON_NAME);
    private final AnchorPane anchorPane = new AnchorPane();
    private final Scene scene = new Scene(scrollPane, WIDTH, HEIGHT);
    private final Stage stage;

    public FamilyTreeMenu(Stage stage, Member root) {
        this.stage = stage;
        this.root = root;
    }

    private void initComponents() {

        scene.getStylesheets().add(getClass().getResource(CSS_PATH).toExternalForm());

        scrollPane.setId(SCROLL_PANE_ID);

        containerElements.setId(CONTAINER_ELEMENTS_ID);

        if (root == null) {
            containerElements.getChildren().addAll(addANewMemberButton);
        } else {
            containerElements.getChildren().addAll(getFamilyTreeGraph(root));
        }

        anchorPane.setId(ANCHOR_PANE_ID);
        anchorPane.getChildren().addAll(containerElements);

        scrollPane.setContent(anchorPane);

        stage.setScene(scene);
        stage.setResizable(false);
    }

    public void start() {
        initComponents();
        this.stage.show();
    }

    public void setAddANewMemberButtonAction(EventHandler<ActionEvent> event) {
        this.addANewMemberButton.setOnAction(event);
    }

    private Node getFamilyTreeGraph(Member root) {
        //TODO: set up the structure for the other members
        Button rootButton = new Button(root.toString());
        rootButton.setUserData(root);
        return rootButton;
    }

    public Stage getStage() {
        return stage;
    }
}
