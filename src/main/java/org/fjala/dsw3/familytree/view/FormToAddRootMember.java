package org.fjala.dsw3.familytree.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.fjala.dsw3.familytree.model.Gender;

public class FormToAddRootMember {

    private static final String CREATION_MEMBER_TITLE = "Add member";
    private static final String CREATE_MEMBER_BUTTON_NAME = "Create member";
    private static final String CANCEL_BUTTON_NAME = "Cancel";
    private static final String NAME_PROMPT_TEXT = "Name";
    private static final String LAST_NAME_PROMPT_TEXT = "Last name";
    private static final String CSS_PATH = "/css/form-to-add-root-member.css";
    private static final String TITLE_LABEL_ID = "title-label";
    private static final String SUBTITLE_LABEL_ID = "subtitle-label";
    private static final String HORIZONTAL_PANE_ID = "horizontal-pane";

    private final VBox containerElements = new VBox();
    private final HBox containerCancelCreateButtons = new HBox();
    private final Label labelTitle = new Label(CREATION_MEMBER_TITLE);
    private final Label labelName = new Label(NAME_PROMPT_TEXT);
    private final TextField nameField = new TextField();
    private final Label labelLastName = new Label(LAST_NAME_PROMPT_TEXT);
    private final TextField lastNameField = new TextField();
    private final DatePicker datePicker = new DatePicker();
    private final ChoiceBox<Gender> genderChoiceBox = new ChoiceBox<>();
    private final Button createMemberButton = new Button(CREATE_MEMBER_BUTTON_NAME);
    private final Button cancelButton = new Button(CANCEL_BUTTON_NAME);
    private final Scene scene = new Scene(containerElements);
    private final Stage stage = new Stage();
    private final Stage parentStage;

    public FormToAddRootMember(Stage parentStage) {
        initComponents();
        this.parentStage = parentStage;
    }

    private void initComponents() {

        scene.getStylesheets().add(getClass().getResource(CSS_PATH).toExternalForm());

        labelTitle.setId(TITLE_LABEL_ID);

        labelName.setId(SUBTITLE_LABEL_ID);

        nameField.setPromptText(NAME_PROMPT_TEXT);

        labelLastName.setId(SUBTITLE_LABEL_ID);

        lastNameField.setPromptText(LAST_NAME_PROMPT_TEXT);

        datePicker.setEditable(false);

        genderChoiceBox.getItems().addAll(Gender.MALE, Gender.FEMALE);

        containerCancelCreateButtons.setId(HORIZONTAL_PANE_ID);
        containerCancelCreateButtons.getChildren().addAll(createMemberButton, cancelButton);

        containerElements.getChildren().addAll(labelTitle, labelName, nameField, labelLastName, lastNameField, datePicker,
                genderChoiceBox, containerCancelCreateButtons);

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setScene(scene);
    }

    public void start() {
        this.stage.show();
    }

    public void setCreateMemberButtonAction(EventHandler<ActionEvent> event) {
        createMemberButton.setOnAction(event);
    }

    public void setCancelButtonAction(EventHandler<ActionEvent> event) {
        cancelButton.setOnAction(event);
    }

    public TextField getNameField() {
        return nameField;
    }

    public TextField getLastNameField() {
        return lastNameField;
    }

    public DatePicker getDatePicker() {
        return datePicker;
    }

    public ChoiceBox<Gender> getGenderChoiceBox() {
        return genderChoiceBox;
    }

    public Stage getStage() {
        return stage;
    }

    public Stage getParentStage() {
        return parentStage;
    }
}
