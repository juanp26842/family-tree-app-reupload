package org.fjala.dsw3.familytree.view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.fjala.dsw3.familytree.exceptions.BlankStringException;
import org.fjala.dsw3.familytree.exceptions.InvalidCharacterException;
import org.fjala.dsw3.familytree.model.FamilyTreeProject;

/**
 * A {@code CreationProjectForm} represents the form with the fields necessary
 * for the creation of a new family tree project.
 */
public class ProjectInfoEditForm {

    public static final String FORM_TITLE = "Info editor of ";
    public static final String PROJECT_NAME = "Project name";
    public static final String PROJECT_DESCRIPTION = "Project description";
    private static final String BLANK_NAME_MESSAGE = "Name field cannot be blank";
    private static final String INVALID_CHARACTER_MESSAGE = "Name contains at least one invalid character";
    public static final String CANCEL = "Cancel";
    public static final String SAVE = "Save";
    public static final int HEIGHT = 500;
    public static final int WIDTH = 400;

    private final Label name = new Label(PROJECT_NAME);
    private final Label description = new Label(PROJECT_DESCRIPTION);
    private final TextField nameField = new TextField();
    private final TextArea descriptionField = new TextArea();
    private final Button cancel = new Button(CANCEL);
    private final Button save = new Button(SAVE);

    private final Stage form;
    private final BorderPane mainPane = new BorderPane();
    private final VBox fieldsPane = new VBox();
    private final BorderPane buttonsPane = new BorderPane();

    private final FamilyTreeProject currentProject;
    private final String previousProjectName;
    private final String previousProjectDescription;

    /**
     * Creates a new {@code CreationProjectForm} and initializes the UI components.
     * @param previousProjectName
     * @param previousProjectDescription
     * @param currentProject
     */
    public ProjectInfoEditForm(String previousProjectName, String previousProjectDescription, Stage form, FamilyTreeProject currentProject) {
        this.form = form;
        this.previousProjectName = previousProjectName;
        this.previousProjectDescription = previousProjectDescription;
        this.currentProject = currentProject;
        this.initComponents();
    }

    /**
     * Adds the fields and buttons in an orderly fashion to the main panel
     * and this is added to the form.
     */
    private void initComponents() {
        nameField.setText(previousProjectName);
        descriptionField.setText(previousProjectDescription);
        descriptionField.setWrapText(true);
        fieldsPane.setSpacing(10);
        fieldsPane.getChildren().addAll(name, nameField, description, descriptionField);

        cancel.setOnAction(event -> {
            ProjectInfoView projectInfoView = new ProjectInfoView(form, currentProject);
            projectInfoView.start();
        });

        save.setOnAction(event -> {
            try {
                currentProject.setName(nameField.getText());
                ConfirmationPopUpView confirmation = new ConfirmationPopUpView(form, this);
                confirmation.start();
            } catch (BlankStringException e) {
                WarningPopUpView warning = new WarningPopUpView(form, BLANK_NAME_MESSAGE);
                warning.start();
            } catch (InvalidCharacterException e){
                WarningPopUpView warning = new WarningPopUpView(form, INVALID_CHARACTER_MESSAGE);
                warning.start();
            }
        });

        buttonsPane.setLeft(cancel);
        buttonsPane.setRight(save);

        mainPane.setCenter(fieldsPane);
        mainPane.setBottom(buttonsPane);
        mainPane.setBorder(new Border(new BorderStroke(Color.TRANSPARENT,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));

        form.setScene(new Scene(mainPane, WIDTH, HEIGHT));
        form.setTitle(FORM_TITLE + previousProjectName);
    }

    public void confirmChanges() {
        ProjectInfoView projectInfoView = new ProjectInfoView(form, currentProject);
        projectInfoView.start();
    }

    public void revertChanges() {
        currentProject.setName(previousProjectName);
    }

    /**
     * The form is made visible on the user's screen
     * and the fields are cleaned
     */
    public void start() {
        this.form.show();
    }
}
