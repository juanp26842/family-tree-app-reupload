package org.fjala.dsw3.familytree.view;

import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.fjala.dsw3.familytree.controller.ProjectManagerController;
import org.fjala.dsw3.familytree.model.FamilyTreeProject;

/**
 * A {@code CreationProjectForm} represents the form with the fields necessary
 * for the creation of a new family tree project.
 */
public class ProjectInfoView {

    public static final String PROJECT_NAME = "Project name:";
    public static final String PROJECT_DESCRIPTION = "Project description:";
    public static final String BACK = "Back";
    public static final String EDIT = "Edit";
    public static final int HEIGHT = 500;
    public static final int WIDTH = 400;

    private final Label name = new Label(PROJECT_NAME);
    private final Label description = new Label(PROJECT_DESCRIPTION);
    private final TextField nameField = new TextField();
    private final TextArea descriptionField = new TextArea();
    private final Button back = new Button(BACK);
    private final Button edit = new Button(EDIT);

    private final Stage form;
    private final VBox centerPane = new VBox();
    private final VBox backPane = new VBox();
    private final GridPane topPane = new GridPane();
    private final Pane drawingPane = new Pane();
    private final BorderPane mainPane = new BorderPane();
    private final FamilyTreeProject currentProject;
    private final String projectName;
    private final String projectDescription;

    /**
     * Creates a new {@code CreationProjectForm} and initializes the UI components.
     */
    public ProjectInfoView(Stage form, FamilyTreeProject currentProject) {
        this.form = form;
        this.projectName = currentProject.getName();
        this.projectDescription = currentProject.getDescription();
        this.currentProject = currentProject;
        this.initComponents();
    }

    /**
     * Adds the fields and buttons in an orderly fashion to the main panel
     * and this is added to the form.
     */
    private void initComponents() {
        topPane.addRow(0,name, nameField,edit);
        topPane.setHgap(15);

        back.setOnAction(event -> {
            ProjectManagerView view = new ProjectManagerView(form);
            ProjectManagerController controller = new ProjectManagerController(view);
            controller.startView();
        });
        edit.setOnAction(event -> {
            ProjectInfoEditForm projectInfoEditForm = new ProjectInfoEditForm(projectName, projectDescription, form, currentProject);
            projectInfoEditForm.start();
        });

        nameField.setText(projectName);
        nameField.setFocusTraversable(false);
        nameField.setEditable(false);

        descriptionField.setText(projectDescription);
        descriptionField.setEditable(false);
        descriptionField.setWrapText(true);
        descriptionField.setPrefHeight(100);

        drawingPane.setPrefHeight(300);
        drawingPane.setCursor(Cursor.HAND);

        backPane.setAlignment(Pos.BOTTOM_LEFT);
        backPane.getChildren().add(back);

        centerPane.setSpacing(10);
        centerPane.getChildren().addAll(topPane, description, descriptionField, drawingPane);

        mainPane.setCenter(centerPane);
        mainPane.setBottom(backPane);
        mainPane.setBorder(new Border(new BorderStroke(Color.TRANSPARENT,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));

        form.setScene(new Scene(mainPane, WIDTH, HEIGHT));
        form.setTitle(projectName);
    }

    /**
     * The form is made visible on the user's screen
     * and the fields are cleaned
     */
    public void start() {
        this.form.show();
    }
}
