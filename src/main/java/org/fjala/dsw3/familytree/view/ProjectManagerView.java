package org.fjala.dsw3.familytree.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * A {@code ProjectManagerView} is the window where existing projects will be displayed
 * and from where new projects can be created
 */
public class ProjectManagerView {

    public static final String WINDOW_TITLE = "Family Tree App";
    public static final String CREATE_NEW_PROJECT_LABEL = "Create new project";
    private static final int WIDTH = 500;
    private static final int HEIGHT = 600;

    private final Button createNewProject = new Button(CREATE_NEW_PROJECT_LABEL);

    private final BorderPane mainPane = new BorderPane();
    private final Stage stage;
    private final Scene scene = new Scene(mainPane, WIDTH, HEIGHT);

    /**
     * Creates a new {@code ProjectManagerView} and initializes the UI components
     * @param stage     where all graphic elements will be placed
     */
    public ProjectManagerView(Stage stage) {
        this.stage = stage;
        this.initComponents();
    }

    /**
     * Adds the fields and buttons in an orderly fashion to the main panel
     * and this is added to the form.
     */
    private void initComponents() {
        mainPane.setCenter(createNewProject);

        stage.setScene(scene);
        stage.setTitle(WINDOW_TITLE);
    }

    /**
     * The window is made visible on the user's screen
     */
    public void start() {
        this.stage.show();
    }

    /**
     * The action to be performed by the createNewProject button is set
     * @param event     to be assigned to the createNewProject button
     */
    public void setCreateProjectAction(EventHandler<ActionEvent> event) {
        this.createNewProject.setOnAction(event);
    }

    public Stage getStage() {
        return this.stage;
    }
}
