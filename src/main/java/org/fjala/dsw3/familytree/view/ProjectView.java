package org.fjala.dsw3.familytree.view;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * A {@code ProjectView} is the window where you will work on a project
 * and all the actions that can be performed on it.
 */
public class ProjectView {

    private static final int WIDTH = 640;
    private static final int HEIGHT = 640;

    private final BorderPane mainPane = new BorderPane();
    private final Stage stage;
    private final Scene scene = new Scene(mainPane, WIDTH, HEIGHT);

    /**
     * Creates a new {@code ProjectView} and initializes the UI components
     * @param primaryStage  main stage
     */
    public ProjectView(Stage primaryStage) {
        this.stage = primaryStage;
    }

    private void initComponents() {
        stage.setScene(scene);
    }

    /**
     * The window is made visible on the user's screen
     */
    public void start() {
        this.initComponents();
        this.stage.show();
    }
}
