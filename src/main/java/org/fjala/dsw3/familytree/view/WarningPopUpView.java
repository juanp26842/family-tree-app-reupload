package org.fjala.dsw3.familytree.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class WarningPopUpView {

    public static final String WARNING_TITLE = "Validation Error";
    public static final String OK = "OK";
    public static final int HEIGHT = 100;
    public static final int WIDTH = 250;

    private final TextArea descriptionField = new TextArea();
    private final Button ok = new Button(OK);

    private final Stage form = new Stage();
    private final VBox mainPane = new VBox();
    private final Stage parentStage;
    private final String warningMessage;

    public WarningPopUpView(Stage parentStage, String warningMessage) {
        this.parentStage = parentStage;
        this.warningMessage = warningMessage;
        initComponents();
    }

    private void initComponents() {
        descriptionField.setWrapText(true);
        descriptionField.setEditable(false);
        descriptionField.setText(warningMessage);
        descriptionField.setFocusTraversable(false);

        ok.setOnAction(event -> {
            form.close();
        });

        mainPane.setAlignment(Pos.BOTTOM_CENTER);

        mainPane.setSpacing(10);
        mainPane.getChildren().addAll(descriptionField, ok);
        mainPane.setBorder(new Border(new BorderStroke(Color.TRANSPARENT,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));

        form.setResizable(false);
        form.initModality(Modality.APPLICATION_MODAL);
        form.initOwner(parentStage);
        form.setScene(new Scene(mainPane, WIDTH, HEIGHT));
        form.setTitle(WARNING_TITLE);
    }

    public void start() {
        this.form.show();
    }
}
