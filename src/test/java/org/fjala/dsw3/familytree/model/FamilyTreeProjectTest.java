package org.fjala.dsw3.familytree.model;

import org.junit.jupiter.api.Test;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FamilyTreeProjectTest {
    @Test
    void setRootMemberShouldSetTheRootMember() {
        String name = "Family";
        String description = "Description";
        FamilyTreeProject familyTree = new FamilyTreeProject(name, description);
        Member member = new Member("Joe", "Due", Gender.MALE, LocalDate.of(2002, 4, 4));

        familyTree.setRootMember(member);

        assertEquals(member, familyTree.getRootMember());
    }

    @Test
    void setRootMemberShouldSetTheRootMemberWhenTheRootMemberIsNull() {
        String name = "Family";
        String description = "Description";
        FamilyTreeProject familyTree = new FamilyTreeProject(name, description);

        familyTree.setRootMember(null);

        assertNull(familyTree.getRootMember());
    }

    @Test
    void familyTreeProjectShouldSetTheGivenAttributesWhenTheyAreCorrect() {
        String name = "O'Connor";
        String description = "The family description";

        FamilyTreeProject familyTreeProject = new FamilyTreeProject(name, description);

        assertEquals(name, familyTreeProject.getName());
        assertEquals(description, familyTreeProject.getDescription());
    }

    @Test
    void familyTreeProjectShouldThrowExceptionWhenTheNameIsNull() {
        String description = "Description";

        assertThrows(IllegalArgumentException.class, () -> new FamilyTreeProject(null,
                description));
    }

    @Test
    void familyTreeProjectShouldThrowExceptionWhenTheNameIsEmpty() {
        String name = "";
        String description = "Description";

        assertThrows(IllegalArgumentException.class, () -> new FamilyTreeProject(name, description));
    }

    @Test
    void familyTreeProjectShouldThrowExceptionWhenTheNameHasOnlyWhiteSpaces() {
        String name = "    ";
        String description = "Description";

        assertThrows(IllegalArgumentException.class, () -> new FamilyTreeProject(name, description));
    }

    @Test
    void familyTreeProjectShouldThrowExceptionWhenTheNameHasSpecialCharacters() {
        String name = "O-Connor";
        String description = "Description";

        assertThrows(IllegalArgumentException.class, () -> new FamilyTreeProject(name, description));
    }
}
