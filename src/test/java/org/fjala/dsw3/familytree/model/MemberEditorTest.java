package org.fjala.dsw3.familytree.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MemberEditorTest {

    private static MemberEditor editorMember = new MemberEditor();

    /**
     * Happy path tests
     */
    @Test
    void editMembersNameShouldSetNicolasNameGivenANameOfNicolas() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("NotNicolas", "Alba", gender, localDate);

        String expectedResult = "Nicolas";

        editorMember.setMember(member);
        editorMember.editMembersName("Nicolas");

        assertEquals(expectedResult, member.getName());
    }

    @Test
    void editMembersNameShouldSetJazminNameGivenANameOfJazmin() {
        LocalDate localDate = null;
        Gender gender = Gender.FEMALE;
        Member member = new Member("NotJazmin", "Choque", gender, localDate);

        String expectedResult = "Jazmin";

        editorMember.setMember(member);
        editorMember.editMembersName("Jazmin");

        assertEquals(expectedResult, member.getName());
    }

    @Test
    void editMembersLastNameShouldSetAvilesLastNameGivenALastNamedAviles() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("Jhonatan", "NotAviles", gender, localDate);

        String expectedResult = "Aviles";

        editorMember.setMember(member);
        editorMember.editMembersLastName("Aviles");

        assertEquals(expectedResult, member.getLastName());
    }

    @Test
    void editMembersLastNameShouldSetBarriosLastNameGivenALastNamedBarrios() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("Sebastian", "NotBarrios", gender, localDate);

        String expectedResult = "Barrios";

        editorMember.setMember(member);
        editorMember.editMembersLastName("Barrios");

        assertEquals(expectedResult, member.getLastName());
    }

    @Test
    void editMembersLastNameShouldSetMirandaLastNameGivenALastNamedMiranda() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("NotCristopher", "NotMiranda", gender, localDate);

        String expectedResult = "Miranda";

        editorMember.setMember(member);
        editorMember.editMembersLastName("Miranda");

        assertEquals(expectedResult, member.getLastName());
    }

    /**
     * Negative tests
     */

    @Test
    void editMembersNameShouldThrowAnIllegalArgumentExceptionGivenAStringWithInitialBlankSpaces() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("Sebastian", "Barrios", gender, localDate);

        editorMember.setMember(member);

        String nameWithInitialBlankSpaces = "   Sebastian";
        String lastNameWithInitialBlankSpaces = "  Barrios";

        assertThrows(IllegalArgumentException.class,
                () -> editorMember.editMembersName(nameWithInitialBlankSpaces));
        assertThrows(IllegalArgumentException.class,
                () -> editorMember.editMembersLastName(lastNameWithInitialBlankSpaces));
    }

    @Test
    void editMembersNameShouldThrowAnIllegalArgumentExceptionGivenAStringWithClosingBlankSpaces() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("Brayan", "Nogales", gender, localDate);

        editorMember.setMember(member);

        String nameWithClosingBlankSpaces = "Sebastian     ";
        String lastNameWithClosingBlankSpaces = "Barrios    ";

        assertThrows(IllegalArgumentException.class,
                () -> editorMember.editMembersName(nameWithClosingBlankSpaces));
        assertThrows(IllegalArgumentException.class,
                () -> editorMember.editMembersLastName(lastNameWithClosingBlankSpaces));
    }

    @Test
    void editMembersNameShouldThrowAnIllegalArgumentExceptionGivenAnEmptyString() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("Cristopher", "Miranda", gender, localDate);

        editorMember.setMember(member);

        String emptyString = "";

        assertThrows(IllegalArgumentException.class, () -> editorMember.editMembersName(emptyString));
        assertThrows(IllegalArgumentException.class, () -> editorMember.editMembersLastName(emptyString));
    }

    @Test
    void editMembersNameShouldThrowAnIllegalArgumentExceptionGivenAnNullString() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("Nicolas", "Alba", gender, localDate);

        editorMember.setMember(member);

        String nullString = null;

        assertThrows(IllegalArgumentException.class, () -> editorMember.editMembersName(nullString));
        assertThrows(IllegalArgumentException.class, () -> editorMember.editMembersLastName(nullString));
    }

    @Test
    void editMembersNameShouldThrowAnIllegalArgumentExceptionGivenAStringWithOpenKeyMathCharacter() {
        LocalDate localDate = null;
        Gender gender = Gender.FEMALE;
        Member member = new Member("Jazmin", "Vargas", gender, localDate);

        editorMember.setMember(member);

        String nameOpenKey = "{Jazmin";
        String lastNameOpenKey = "{Vargas";

        assertThrows(IllegalArgumentException.class, () -> editorMember.editMembersName(nameOpenKey));
        assertThrows(IllegalArgumentException.class, () -> editorMember.editMembersLastName(lastNameOpenKey));
    }

    @Test
    void editMembersNameShouldThrowAnIllegalArgumentExceptionGivenAStringWithLockKeyMathCharacter() {
        LocalDate localDate = null;
        Gender gender = Gender.MALE;
        Member member = new Member("Jhonatan", "Aviles", gender, localDate);

        editorMember.setMember(member);

        String nameLockKey = "Jhonatan}";
        String lastNameLockKey = "Aviles}";

        assertThrows(IllegalArgumentException.class, () -> editorMember.editMembersName(nameLockKey));
        assertThrows(IllegalArgumentException.class, () -> editorMember.editMembersName(lastNameLockKey));
    }

}
