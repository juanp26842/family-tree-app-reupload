package org.fjala.dsw3.familytree.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MemberTest {
    @Test
    void constructorShouldSetTheGivenAttributesWhenTheyAreCorrect() {
        String name = "John";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        Member member = new Member(name, lastName, gender, birthDate);

        assertEquals(name, member.getName());
        assertEquals(lastName, member.getLastName());
        assertEquals(birthDate, member.getBirthDate());
        assertEquals(gender, member.getGender());
    }

    @Test
    void constructorShouldThrowExceptionWhenTheNameIsNull() {
        String name = null;
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldThrowExceptionWhenTheNameIsBlank() {
        String name = "           ";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldThrowExceptionWhenTheNameIsEmpty() {
        String name = "";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{", "}", "(", ")", "[", "]", "=",
            "+", "*", "_", "-", "/", "\\", "|",
            "<", ">", "?", "%", "$", "!"
    })
    void constructorShouldThrowExceptionWhenTheNameContainsAMathCharacter(String mathCharacter) {
        String name = "John" + mathCharacter;
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheNameHasJapaneseCharacters() {
        String name = "\u5cf6\u888b";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheNameHasRussianCharacters() {
        String name = "\u0410\u0444\u0430\u043d\u0430\u0441\u0438\u0439";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheNameHasAccentedCharacters() {
        String name = "Nicol\u00e1s";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheNameHasUpperCaseAccentedCharacters() {
        String name = "\u00c1gata";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldThrowExceptionWhenTheLastNameIsNull() {
        String name = "John";
        String lastName = null;
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldThrowExceptionWhenTheLastNameIsEmpty() {
        String name = "John";
        String lastName = "";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldThrowExceptionWhenTheLastNameIsBlank() {
        String name = "John";
        String lastName = "           ";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{", "}", "(", ")", "[", "]", "=",
            "+", "*", "_", "-", "/", "\\", "|",
            "<", ">", "?", "%", "$", "!"
    })
    void constructorShouldThrowExceptionWhenTheLastNameContainsAMathCharacter(String mathCharacter) {
        String name = "John";
        String lastName = "Due" + mathCharacter;
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheLastNameContainsSimpleQuoteMark() {
        String name = "John";
        String lastName = "O'Higgins";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheLastNameHasJapeneseCharacters() {
        String name = "John";
        String lastName = "\u6f22\u5b57";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheLastNameHasRussianCharacters() {
        String name = "John";
        String lastName = "\u041d\u043e\u0432\u0438\u043a\u043e\u0301\u0432";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheLastNameHasAccentedCharacters() {
        String name = "John";
        String lastName = "Fern\u00e1ndez";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheLastNameHasUpperCaseAccentedCharacters() {
        String name = "John";
        String lastName = "\u00c1lvares";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldNotThrowExceptionWhenTheBirthDateIsNull() {
        String name = "John";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = null;

        assertDoesNotThrow(() -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldThrowExceptionWhenTheBirthDateIsFuture() {
        String name = "John";
        String lastName = "Due";
        Gender gender = Gender.MALE;
        LocalDate birthDate = LocalDate.now().plusYears(1);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }

    @Test
    void constructorShouldThrowExceptionWhenTheGenderIsNull() {
        String name = "John";
        String lastName = "Due";
        Gender gender = null;
        LocalDate birthDate = LocalDate.of(2002, 4, 4);

        assertThrows(IllegalArgumentException.class, () -> new Member(name, lastName, gender, birthDate));
    }
}