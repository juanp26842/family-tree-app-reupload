package org.fjala.dsw3.familytree.services;

import org.fjala.dsw3.familytree.services.JsonFileWriter;
import org.fjala.dsw3.familytree.model.FamilyTreeProject;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class NameOfFileTest {

    @Test
    public void deleteSpecialCharactersShouldReturnAStringValidWhenPutSpecialCharacters() {
        FamilyTreeProject familyTree = new FamilyTreeProject("Fa·|@#~½¬{[]]}!·$%&/()=?¿milyPere@€€¶¶←↓→[]}{~z"
                , "This is a Tree of Family Project");
        JsonFileWriter jsonFileWriter = new JsonFileWriter(familyTree, "");

        String actual =
                jsonFileWriter.deleteSpecialCharacters(familyTree.getName());
        String expected = "FamilyPerez";

        assertEquals(expected, actual);
    }

    @Test
    public void deleteSpecialCharactersShouldReturnAStringValidWhenPutCharactersAlphanumerics() {
        FamilyTreeProject familyTree = new FamilyTreeProject("Fa1m2i3l4y5P6e7r8e9z0"
                , "This is a Tree of Family Project");
        JsonFileWriter jsonFileWriter = new JsonFileWriter(familyTree, "");

        String actual =
                jsonFileWriter.deleteSpecialCharacters(familyTree.getName());
        String expected = "FamilyPerez";

        assertEquals(expected, actual);
    }

    @Test
    public void deleteSpecialCharactersShouldReturnAStringValidWhenPutInvalidCharactersForAFile() {
        FamilyTreeProject familyTree = new FamilyTreeProject("Family/><*:|Perez"
                , "This is a Tree of Family Project");
        JsonFileWriter jsonFileWriter = new JsonFileWriter(familyTree, "");

        String actual =
                jsonFileWriter.deleteSpecialCharacters(familyTree.getName());
        String expected = "FamilyPerez";

        assertEquals(expected, actual);
    }
}
