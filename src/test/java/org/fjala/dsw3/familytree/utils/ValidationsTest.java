package org.fjala.dsw3.familytree.utils;

import org.fjala.dsw3.familytree.exceptions.BlankStringException;
import org.fjala.dsw3.familytree.exceptions.FutureDateException;
import org.fjala.dsw3.familytree.exceptions.InvalidCharacterException;
import org.fjala.dsw3.familytree.exceptions.NullValueException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidationsTest {
    @Test
    void validateNonNullValueShouldThrowNullValueExceptionWhenInputIsNull() {
        String fieldName = "Name";
        Object value = null;

        NullValueException exception = assertThrows(NullValueException.class, () -> {
            Validations.validateNonNullValue(value, fieldName);
        });

        assertEquals(fieldName, exception.getFieldName());
    }

    @Test
    void validateNonNullValueShouldNotThrowNullValueExceptionWhenInputIsNotNull() {
        String fieldName = "Name";
        Object value = "Joe";

        assertDoesNotThrow(() -> Validations.validateNonNullValue(value, fieldName));
    }

    @Test
    void validateNotFutureDateShouldThrowFutureDateExceptionWhenValueIsAFutureDate() {
        LocalDate futureDate = LocalDate.now().plusYears(1);
        String fieldName = "Birth date";

        FutureDateException exception = assertThrows(FutureDateException.class, () -> {
            Validations.validateNotFutureDate(futureDate, fieldName);
        });

        assertEquals(fieldName, exception.getFieldName());
    }

    @Test
    void validateNotFutureDateShouldNotThrowExceptionWhenTheDateIsNotAFutureDate() {
        LocalDate pastDate = LocalDate.of(2002, 4, 4);
        String fieldName = "Birth date";

        assertDoesNotThrow(() -> Validations.validateNotFutureDate(pastDate, fieldName));
    }

    @Test
    void validateNotBlankStringShouldThrowExceptionWhenTheValueIsBlank() {
        String value = "     ";
        String fieldName = "Name";

        BlankStringException exception = assertThrows(BlankStringException.class, () -> {
            Validations.validateNotBlankString(value, fieldName);
        });

        assertEquals(fieldName, exception.getFieldName());
    }

    @Test
    void validateNotBlankStringShouldThrowExceptionWhenTheValueIsEmpty() {
        String value = "";
        String fieldName = "Name";

        BlankStringException exception = assertThrows(BlankStringException.class, () -> {
            Validations.validateNotBlankString(value, fieldName);
        });

        assertEquals(fieldName, exception.getFieldName());
    }

    @Test
    void validateNotBlankStringShouldNowThrowExceptionWhenTheValueIsNotBlank() {
        String value = "NotBlankString";
        String fieldName = "Name";

        assertDoesNotThrow(() -> Validations.validateNotBlankString(value, fieldName));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{", "}", "(", ")", "[", "]", "=",
            "+", "*", "_", "-", "/", "\\", "|",
            "<", ">", "?", "%", "$", "!"
    })
    void validateValidCharactersShouldThrowExceptionWhenTheValueContainsAInvalidCharacter(String mathCharacter) {
        String value = "Value" + mathCharacter;
        String fieldName = "Last Name";

        InvalidCharacterException exception = assertThrows(InvalidCharacterException.class, () -> {
            Validations.validateValidCharacters(value, fieldName);
        });

        assertEquals(fieldName, exception.getFieldName());
    }

    @Test
    void validateValidCharactersShouldNotThrowExceptionWhenTheValueDoesNotContainsAInvalidCharacter() {
        String value = "Joe";
        String fieldName = "Name";

        assertDoesNotThrow(() -> Validations.validateValidCharacters(value, fieldName));
    }
}